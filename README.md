
wald:test - A framework for Literate API testing
================================================

Usage
-----

Use `bin/wald-test` to run your tests and generate html files with results.

The result .html files will automatically reload themselves if new test results are
available, though this only works if you're viewing them via a webserver (file://
doesn't work for autoreloading).  You can specify --output-directory to save the
results to a private or public www dir, or you can run `bin/wald-test --start` to just
start a webserver to view the test results.

How to write a test
-------------------

FIXME: write this chapter.


The Test Runner
===============

To run your API tests wald:test performs various steps, you don't typically need to
know about these, but it may help when you're having trouble.

Let's go through each of the steps:

extract (lib/extract.py)
------------------------

First, the output folder is created and tests are extracted from your input and saved
as a .json file to the output folder.  So, for example if you have an input file with
multiple tests called `examples/LicenseDB/static.md`, this step will generate
`wald.test.results/LicenseDB/static.test.json`.


runtests (lib/runtests.py)
--------------------------

runtests reads each test description and performs the request as described using the python
[requests library](http://www.python-requests.org), and then saves the results to a *.results.json
file, which is a copy of *.test.json with test results added to it.

Using the example above, this step will generate `wald.test.results/LicenseDB/static.results.json`.

output (lib/output.py)
----------------------

Finally the test results are combined with an HTML template to generate the test
result pages.  So that test results can be read as part of the description or story of
the test.

Using the example above, this step will generate `wald.test.results/LicenseDB/static.html`.


Testing wald:test
=================

wald:test itself is tested (to some extent) using snapshots of the expected output.
After making changes to the code, generate new test results with:

    bin/wald-test

This will by default run the example tests in the tests/ folder.  After running the
tests, the test results snapshots may need to be updated.  This can be done with:

    UPDATE=1 npm test

Any changes where no change in the output is expected should be verified using a
regular test run:

    npm test


License
=======

Copyright 2017  Kuno Woudt <kuno@frob.nl>

This program is free software: you can redistribute it and/or modify
it under the terms of copyleft-next 0.3.1.  See
[copyleft-next-0.3.1.txt](copyleft-next-0.3.1.txt).
