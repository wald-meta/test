/*
 * This file is part of wald:test, A framework for Literate API testing
 * Copyright 2017  Kuno Woudt <kuno@frob.nl>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of copyleft-next 0.3.1.  See copyleft-next-0.3.1.txt
 */

'use strict';

const fs = require('fs');
const snapshot = require('snap-shot');

const files = [
    'wald.test.results/index.json',
    'wald.test.results/tree/01-intro.test.json',
    'wald.test.results/tree/01-intro.results.json',
    'wald.test.results/tree/02-ssl/01-certificates/expired.results.json',
    'wald.test.results/tree/02-ssl/01-certificates/expired.test.json',
    'wald.test.results/tree/02-ssl/01-certificates/good.results.json',
    'wald.test.results/tree/02-ssl/01-certificates/good.test.json',
    'wald.test.results/tree/02-ssl/02-hsts.results.json',
    'wald.test.results/tree/02-ssl/02-hsts.test.json',
    'wald.test.results/tree/03-http-methods.results.json',
    'wald.test.results/tree/03-http-methods.test.json',
];

function removeVolatile(data) {
    // We need to remove changes which are expected.  This mostly are date fields, but also
    // certain HTTP headers, and probably in the future response bodies.
    if (data.metadata) {
        delete data.metadata.extractDate;
        delete data.metadata.run;
        delete data.metadata.date;
        delete data.metadata.version;
    }

    if (data.input && data.input.filename) {
        // FIXME: reduce to a relative path instead of deleting this entirely.
        delete data.input.filename;
    }

    if (data.tests) {
        const testCopy = data.tests;
        delete data.tests;

        data.tests = {};

        testCopy.forEach(test => {
            delete test.resultDate;
            if (test.response) {
                if (test.response.headers) {
                    delete test.response.headers.Date;
                    delete test.response.headers.ETag;
                    delete test.response.headers.Server;
                    delete test.response.headers.Via;
                    delete test.response.headers['Content-Length'];
                    delete test.response.headers['Last-Modified'];
                    delete test.response.headers['X-Processed-Time'];
                }
                delete test.response.body;
                delete test.response.elapsed;
            }
            data.tests[test.id] = test;
        });
    }

    return data;
}

suite('wald:test example tests', () => {
    files.forEach(filename => {
        test(filename, function testResults() {
            snapshot(removeVolatile(JSON.parse(fs.readFileSync(filename))));
        });
    });
});
