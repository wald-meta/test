#!/usr/bin/env python3

# This file is part of wald:test, A framework for Literate API testing
# Copyright 2017  Kuno Woudt <kuno@frob.nl>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of copyleft-next 0.3.1.  See copyleft-next-0.3.1.txt

import codecs
import collections
import CommonMark
import datetime
import hashlib
import json
import os
import os.path
import re
import sys
import yaml

CodeBlock = collections.namedtuple('CodeBlock', 'node data')
HeadingBlock = collections.namedtuple('HeadingBlock', 'node title')


def is_metadata(data):
    return "idPrefix" in data


def code_blocks(nodes):
    return filter(lambda event: event.t == 'code_block', nodes)


def heading_blocks(nodes):
    return filter(lambda event: event.t == 'heading', nodes)


def all_nodes(body):
    parser = CommonMark.Parser()
    ast = parser.parse(body)
    walker = ast.walker()

    for (event, _) in walker:
        yield event


def extract_text(node):
    w = node.walker()

    ret = ""
    for (node, _) in w:
        if node.t == 'text':
            ret = ret + node.literal

    return ret


def parse_code_blocks(nodes):
    for n in nodes:
        data = None

        try:
            if n.info == 'json':
                yield CodeBlock(n, json.loads(n.literal))
            elif n.info == 'yaml':
                yield CodeBlock(n, yaml.safe_load(n.literal))
        except:
            # FIXME: output to stderr
            # FIXME: add details (filename + line number)
            print("WARNING: could not parse node")


def parse_heading_blocks(nodes):
    for n in nodes:
        yield HeadingBlock(n, extract_text(n))


def find_heading(headings, linepos):
    last_heading = None
    for h in headings:
        if h.node.sourcepos[0][0] > linepos:
            return last_heading

        last_heading = h.title

    return last_heading


def construct_tests(headings, blocks):
    metadata = {}
    tests = []

    # FIXME: assign identifiers to each test / check

    current_test = {}
    expecting_test = True
    for code_block in blocks:
        b = code_block.data
        n = code_block.node

        if is_metadata(b):
            metadata = b

        elif expecting_test:
            # FIXME: check if a HTTP verb is in the test
            expecting_test = False

            current_test['name'] = find_heading(headings, n.sourcepos[0][0])
            current_test['request'] = b
            current_test['request_sourcepos'] = n.sourcepos
        else:
            # FIXME: check if status is in the test?
            # (though status probably shouldn't be mandatory)
            expecting_test = True

            current_test['expected'] = b
            current_test['expected_sourcepos'] = n.sourcepos
            tests.append(current_test)
            current_test = {}

    return {"metadata": metadata, "tests": tests}


def disambiguate_duplicate_names(data):
    seen = {}
    dupes = {}

    for t in data["tests"]:
        if t["name"] in seen:
            dupes[t["name"]] = 1
        else:
            seen[t["name"]] = True

    for t in data["tests"]:
        if t["name"] in dupes:
            name = t["name"]
            t["name"] = "%s, part %d" % (name, dupes[name])
            dupes[name] += 1


def process(filename):
    with open(filename, mode="rb") as f:
        body = f.read()

    input_file_hash = hashlib.sha256(body).hexdigest()
    bodyStr = body.decode("UTF-8")

    headings = list(parse_heading_blocks(heading_blocks(all_nodes(bodyStr))))
    data = construct_tests(headings,
                           parse_code_blocks(code_blocks(all_nodes(bodyStr))))

    disambiguate_duplicate_names(data)

    data["input"] = {
        "filename": os.path.realpath(filename),
        "sha256": input_file_hash,
    }

    if not "metadata" in data:
        data["metadata"] = {}

    data["metadata"]["extractDate"] = datetime.datetime.utcnow().isoformat()

    id_prefix = "test-"
    if "idPrefix" in data["metadata"]:
        id_prefix = data["metadata"]["idPrefix"]

    max_count = len(data["tests"])

    id_width = len(str(max_count))
    id = 0
    for t in data["tests"]:
        t["id"] = id_prefix + str(id).zfill(id_width)
        id += 1

    return data


def save(filename, data, output_root):
    os.makedirs(os.path.dirname(filename), exist_ok=True)

    data_str = json.dumps(
        data, sort_keys=True, indent=4, separators=(',', ': '))
    with codecs.open(filename, mode="wb", encoding="UTF-8") as f:
        f.write(data_str + "\n")

    display_filename = os.path.relpath(filename, os.path.dirname(output_root))

    print("Saved %s (%d tests)" % (display_filename, len(data["tests"])))


def extract_dir(current_dir, output_root):
    parent_dir = os.path.dirname(current_dir)
    print("Changed to", parent_dir)
    os.chdir(parent_dir)

    output_files = []

    for root, dirs, files in os.walk(os.path.basename(current_dir)):
        for f in files:
            if f.endswith('.md') and not f.startswith('.'):
                input_filename = os.path.join(root, f)
                output_filename = os.path.join(output_root,
                                               os.path.relpath(
                                                   re.sub(
                                                       '\.md$', '.test.json',
                                                       input_filename),
                                                   current_dir))

                save(output_filename, process(input_filename), output_root)
                output_files.append(os.path.realpath(output_filename))

    return output_files


if __name__ == "__main__":
    dirs = []
    if len(sys.argv) > 1:
        dirs = list(map(os.path.realpath, sys.argv[1:]))

    for current_dir in dirs:
        extract_dir(current_dir)
