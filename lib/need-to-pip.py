#!/usr/bin/env python3

# This file is part of wald:test, A framework for Literate API testing
# Copyright 2017  Kuno Woudt <kuno@frob.nl>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of copyleft-next 0.3.1.  See copyleft-next-0.3.1.txt

import codecs
import os
import os.path
import pkg_resources
import sys

wald_test_root = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

with codecs.open(
        os.path.join(wald_test_root, 'requirements.txt'),
        encoding="UTF-8") as f:
    requirements = [line.strip() for line in f.readlines()]

try:
    pkg_resources.require(requirements)
except:
    sys.exit(0)

sys.exit(1)
