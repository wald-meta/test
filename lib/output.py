#!/usr/bin/env python3

# This file is part of wald:test, A framework for Literate API testing
# Copyright 2017  Kuno Woudt <kuno@frob.nl>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of copyleft-next 0.3.1.  See copyleft-next-0.3.1.txt

import codecs
import collections
import CommonMark
import datetime
import hashlib
import json
import os
import os.path
import re
import simplejson
import sys
import yaml

from simplejson.encoder import JSONEncoderForHTML
from CommonMark.common import escape_xml

_wald_test_root = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
_result_template_file = os.path.join(_wald_test_root, 'template',
                                     'test-result.html')
_index_template_file = os.path.join(_wald_test_root, 'template', 'index.html')


def identifiers(test_results):
    ret = {}

    for t in test_results["tests"]:
        test_id = t["id"]
        expected_id = test_id + "-expected"

        ret[str(t["request_sourcepos"])] = test_id
        ret[str(t["expected_sourcepos"])] = expected_id

    return ret


class WaldRenderer(CommonMark.HtmlRenderer):
    def test_results(self, data):
        self._test_results = data
        self._id_mapping = identifiers(data)

    def code_block(self, node, entering):
        info_words = node.info.split() if node.info else []
        attrs = self.attrs(node)
        if len(info_words) > 0 and len(info_words[0]) > 0:
            attrs.append(
                ['class', 'language-' + escape_xml(info_words[0], True)])

        # Add a wrapper div into which we can render the React element
        wrapper_attrs = []
        pos = str(node.sourcepos)
        if pos in self._id_mapping:
            block_id = self._id_mapping[pos]
            wrapper_attrs.append(['id', escape_xml(block_id, True)])
            wrapper_attrs.append(['data-wald-test', "true"])
            if block_id.endswith("-expected"):
                wrapper_attrs.append(['data-expected', "true"])
            else:
                wrapper_attrs.append(['data-expected', "false"])

        self.cr()
        self.tag('div', wrapper_attrs)
        self.tag('pre')
        self.tag('code', attrs)
        self.out(node.literal)
        self.tag('/code')
        self.tag('/pre')
        self.tag('/div')
        self.cr()


def read_and_verify(filename, sha256):
    with open(filename, mode="rb") as f:
        body = f.read()

    input_file_hash = hashlib.sha256(body).hexdigest()
    if input_file_hash != sha256:
        print(
            "WARNING: Source file \"%s\" doesn't match results, please re-run tests"
            % (filename))
        return None

    return body


def extract_text(node):
    w = node.walker()

    ret = ""
    for (node, _) in w:
        if node.t == 'text':
            ret = ret + node.literal

    return ret


def get_document_title(ast):
    walker = ast.walker()

    for (event, _) in walker:
        if event.t == 'heading':
            return extract_text(event)

    return None


def generate_output(output_root, result_filename):
    with codecs.open(result_filename, encoding="UTF-8") as f:
        body = f.read()

    data = json.loads(body)
    test_source = read_and_verify(data["input"]["filename"],
                                  data["input"]["sha256"])
    if not test_source:
        return None

    parser = CommonMark.Parser()
    renderer = WaldRenderer()
    renderer.test_results(data)

    ast = parser.parse(test_source.decode("UTF-8"))
    title = get_document_title(ast)
    html = renderer.render(ast)

    output_filename = re.sub('\.results\.json$', '.html', result_filename)

    template = ''
    with codecs.open(_result_template_file, encoding="UTF-8") as f:
        template = f.read()

    now = datetime.datetime.utcnow()

    # FIXME: source_file should maybe contain more context, in theory we should know
    # the test root and only cut off that part instead of using basename.
    source_file = os.path.basename(data["input"]["filename"])

    # FIXME: title, like source_file, should be more accurate
    # FIXME: root should not be hardcoded, but set relative to how deep the test result is

    rel_path_to_root = os.path.relpath(output_root,
                                       os.path.dirname(result_filename))

    failed = False
    for test in data["tests"]:
        for check in test["checks"]:
            if not check["ok"]:
                failed = True

    skipped = False
    for test in data["tests"]:
        for check in test["checks"]:
            if check["skipped"]:
                skipped = True

    final_output = template.format(
        date=now.strftime("%Y-%m-%d %H:%M:%S UTC"),
        isodate=now.isoformat(),
        root=rel_path_to_root,
        title=title,
        sourceFile=source_file,
        testDocument=html,
        testResults=simplejson.dumps(
            data, cls=JSONEncoderForHTML, sort_keys=True, indent=4),
    )

    final_filename = os.path.relpath(output_filename, output_root)
    display_filename = os.path.relpath(output_filename,
                                       os.path.dirname(output_root))
    with codecs.open(output_filename, mode="wb", encoding="UTF-8") as f:
        f.write(final_output)
        print("Saved %s" % (display_filename))

    return collections.namedtuple("output", "filename title ok skipped")(
        final_filename, title, not failed, skipped)


def generate_index(output_root, index_data, app_version):
    index_data_filename = os.path.join(output_root, "index.json")
    index_html_filename = os.path.join(output_root, "index.html")
    index_output = []
    for i in index_data:
        index_output.append({
            "filename": i.filename,
            "title": i.title,
            "ok": i.ok,
            "skipped": i.skipped
        })

    index_output.sort(key=lambda i: i["filename"])

    run_count = 0
    try:
        with codecs.open(
                index_data_filename, mode="rb", encoding="UTF-8") as f:
            prev_index_body = f.read()
            prev_index = json.loads(prev_index_body)
            if "metadata" in prev_index:
                run_count = prev_index["metadata"]["run"] + 1
    except:
        run_count = 1

    now = datetime.datetime.utcnow()

    final_data = {
        "index": index_output,
        "metadata": {
            "run": run_count,
            "date": now.isoformat(),
            "version": app_version
        }
    }

    with codecs.open(index_data_filename, mode="wb", encoding="UTF-8") as f:
        f.write(json.dumps(final_data, sort_keys=True, indent=4))

    template = ''
    with codecs.open(_index_template_file, encoding="UTF-8") as f:
        template = f.read()

    firstResult = ''
    if len(index_output):
        firstResult = index_output[0]['filename']

    index_html = template.format(firstResult=firstResult)
    with codecs.open(index_html_filename, mode="wb", encoding="UTF-8") as f:
        f.write(index_html)
