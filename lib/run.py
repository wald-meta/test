#!/usr/bin/env python3

# This file is part of wald:test, A framework for Literate API testing
# Copyright 2017  Kuno Woudt <kuno@frob.nl>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of copyleft-next 0.3.1.  See copyleft-next-0.3.1.txt

import argparse
import codecs
import CommonMark
import contextlib
import daemon
import daemon.runner
import http.server
import json
import os
import os.path
import re
import shutil
import signal
import socketserver
import sys
import yaml

from extract import extract_dir
from output import generate_index, generate_output
from runtests import run_test_file

wald_test_root = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

with codecs.open(
        os.path.join(wald_test_root, 'package.json'), encoding="UTF-8") as f:
    package_json = json.loads(f.read())
    version = package_json['version']

app_version = "wald:test/" + version
user_agent = app_version + " -- https://gitlab.com/wald-meta/test"

parser = argparse.ArgumentParser(
    description='wald:test test runner',
    epilog=(
        "wald:test is free software, you can redistribute it and/or modify\n" +
        " it under the terms of copyleft-next 0.3.1.\n"),
    prog='wald-test')
parser.add_argument('--version', action='version', version=app_version)
parser.add_argument(
    '--start',
    action='store_true',
    default=False,
    help='Start a webserver to serve the test output')
parser.add_argument(
    '--stop',
    action='store_true',
    default=False,
    help='Stop the webserver which is serving the test output')
parser.add_argument(
    'directories', nargs='*', help='one or more directories with tests to run')
parser.add_argument(
    '--output-directory',
    default='wald.test.results',
    help='override where test results are saved')
parser.add_argument('--url', help='override baseUrl for all tests')
args = parser.parse_args()

if len(args.directories) > 0:
    dirs = list(map(os.path.realpath, args.directories))
elif os.path.isdir("test"):
    dirs = [os.path.realpath("test")]
elif os.path.isdir("tests"):
    dirs = [os.path.realpath("tests")]
else:
    print("ERROR: no test folder found or specified")
    sys.exit(1)


def copy_helper_files(target_root):
    files = {
        "css": [
            'node_modules/highlight.js/src/styles/tomorrow-night.css',
            'node_modules/bootswatch/slate/bootstrap.css',
            'template/test-result.css',
        ],
        "js": [
            'node_modules/highlight.js/build/highlight.pack.js',
            'node_modules/jquery/dist/jquery.js',
            'node_modules/bootstrap/dist/js/bootstrap.js',
            'node_modules/react/dist/react-with-addons.js',
            'node_modules/react-dom/dist/react-dom.js',
            'template/test-result.js',
        ]
    }

    for assetType in ["js", "css"]:
        for f in files[assetType]:
            src = os.path.join(wald_test_root, f)
            dst = os.path.join(target_root, assetType, os.path.basename(f))

            os.makedirs(os.path.dirname(dst), exist_ok=True)

            shutil.copy(src, dst)


def serve_pid_file():
    basename = 'wald-test-serve.pid'
    filename = None

    if "XDG_RUNTIME_DIR" in os.environ:
        filename = os.path.join(os.environ["XDG_RUNTIME_DIR"], basename)
    else:
        filename = os.path.join('/tmp', basename)

    timeout = 2  # in seconds?
    return daemon.runner.make_pidlockfile(filename, timeout)


def serve_start(args, dirs):
    # FIXME: currently only the last specified path is served, perhaps serve
    # a common root instead?  Or just warn if multiple are specified?
    for current_dir in dirs:
        output_root = os.path.join(
            os.path.dirname(os.path.realpath(current_dir)),
            args.output_directory)

    # os.chdir(output_root)

    port = 7668  # LD, Linked Data
    handler = http.server.SimpleHTTPRequestHandler
    httpd = socketserver.TCPServer(("", port), handler)

    print("Open http://localhost:%d to see your test results" % (port))

    daemon_context = daemon.DaemonContext()
    daemon_context.files_preserve = [httpd.fileno()]
    daemon_context.working_directory = output_root
    daemon_context.pidfile = serve_pid_file()

    with daemon_context:
        httpd.serve_forever()


def process_exists(pid):
    try:
        os.kill(pid, 0)
    except ProcessLookupError:
        return False

    return True


def serve_stop(args, dirs):
    pidfile = serve_pid_file()
    if not pidfile.is_locked():
        print("wald-test server is not running")
        sys.exit(0)

    pid = pidfile.read_pid()
    if not pid:
        print("wald-test server is not running")
        sys.exit(0)

    if process_exists(pid):
        os.kill(pid, signal.SIGTERM)
        print("wald-test server stopped")
    else:
        pidfile.break_lock()
        print("wald-test server is not running, cleaned up pid file")

    sys.exit(0)


# TODO:
# - [x] output directory
# - [ ] override base url
# - [ ] don't use template and node_modules folders in output directory, use js/ and css/
# - [x] gitlab ci setup


def run_tests(args, dirs):
    for current_dir in dirs:
        output_root = os.path.join(
            os.path.dirname(os.path.realpath(current_dir)),
            args.output_directory)
        copy_helper_files(output_root)

        test_files = extract_dir(current_dir, output_root)

        run_options = {}
        if args.url:
            run_options['baseUrl'] = args.url

        result_files = []
        for t in test_files:
            result_files.append(
                run_test_file(
                    output_root, t, user_agent=user_agent,
                    options=run_options))

        index = []
        for r in result_files:
            index.append(generate_output(output_root, r))

        generate_index(output_root, index, app_version)


if args.start:
    serve_start(args, dirs)
elif args.stop:
    serve_stop(args, dirs)
else:
    run_tests(args, dirs)
