#!/usr/bin/env python3

# This file is part of wald:test, A framework for Literate API testing
# Copyright 2017  Kuno Woudt <kuno@frob.nl>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of copyleft-next 0.3.1.  See copyleft-next-0.3.1.txt

import codecs
import collections
import datetime
import json
import os
import os.path
import re
import requests
import sys
import urllib
import urllib.parse

from dotted.utils import dot_json
from json import JSONEncoder
from sre_compile import isstring


def serialize_response(response):
    return {
        "statusCode": response.status_code,
        "status": "%d %s" % (response.status_code, response.reason),
        "cookies": dict(response.cookies),
        "elapsed": response.elapsed.total_seconds() *
        1000,  ## elapsed time in milliseconds
        "headers": dict(response.headers),
        "body": response.content.decode("UTF-8")
    }


class WaldTestEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, requests.Response):
            return serialize_response(obj)
        elif isinstance(obj, Exception):
            return {
                "headers": {
                    "Exception": obj.__class__.__name__
                },
                "body": str(obj)
            }
        else:
            return JSONEncoder.default(self, obj)


class FailedTest(Exception):
    """Base class for a test failing for any reason"""
    pass


class SkippedTest(Exception):
    """Base class for a test skipped for any reason"""
    pass


def to_sentence(value, join_word=" and "):
    value = list(value)  # make a copy, so we can safely modify it

    if len(value) > 1:
        item = value.pop()
        return ", ".join(map(lambda v: str(v), value)) + join_word + str(item)
    else:
        return item


def run_test(metadata, request, session):
    method = (list(filter(lambda s: s.isupper(), request.keys())) or [None])[0]
    if not method:
        # FIXME: add context about which test failed.
        raise FailedTest("No HTTP method specified in request")

    if "skip" in request:
        raise SkippedTest("test skipped")
    elif "skip" in metadata:
        raise SkippedTest("file skipped")

    baseUrl = request["baseUrl"] if "baseUrl" in request else metadata[
        "baseUrl"]
    url = urllib.parse.urljoin(baseUrl, request[method])

    data = None
    if 'body' in request:
        data = request["body"]

    headers = None
    if "headers" in request:
        headers = request["headers"]

    return session.request(
        method,
        url,
        data=data,
        allow_redirects=False,
        headers=headers,
        timeout=5)


def as_iterable(value):
    if isstring(value):
        return [value]

    if isinstance(value, collections.Sequence):
        return value

    return [value]


def exception_actual(ex):
    return "Exception: " + ex.__class__.__name__


def describe_exception(expected):
    if len(expected) > 1:
        return "one of: " + to_sentence(expected, " or ")
    else:
        return expected[0]


def describe_status(expected):
    for v in expected:
        if isstring(v):
            print(
                "WARNING: expected status value should be integer or list of integers"
            )

    if len(expected) > 1:
        return "one of: " + to_sentence(expected, " or ")
    else:
        return str(expected[0])


def validate_exception(response, expected):
    expected = as_iterable(expected)

    name = response.__class__.__name__

    ret = {
        "actual": exception_actual(response),
        "expected": describe_exception(expected),
        "real-expected": expected,
        "ok": name in expected,
        "type": "status",
        "skipped": False,
        "variable": "code",
    }

    if isinstance(response, SkippedTest):
        ret["ok"] = True
        ret["skipped"] = True

    return ret


def validate_status(response, expected):
    expected = as_iterable(expected)

    ret = {
        "expected": describe_status(expected),
        "real-expected": expected,
        "type": "status",
        "variable": "code",
        "skipped": False,
    }

    if isinstance(response, SkippedTest):
        ret["actual"] = exception_actual(response)
        ret["ok"] = True
        ret["skipped"] = True
    elif isinstance(response, Exception):
        ret["actual"] = exception_actual(response)
        ret["ok"] = False
    else:
        ret["actual"] = response["status"]
        ret["ok"] = response["statusCode"] in expected

    return ret


def validate_header(response, expected):
    headers = list(expected.keys())
    if len(headers) != 1:
        print("ERROR: malformed header test")
        sys.exit(1)

    # FIXME: support multiple header checks
    header = headers[0]
    expectedStr = ""
    if isstring(expected[header]):
        expectedStr = expected[header]
    else:
        expectedStr = ", ".join(
            ["%s: %s" % (k, v) for (k, v) in sorted(expected[header].items())])

    ret = {
        "expected": expectedStr,
        "type": "header",
        "skipped": False,
        "variable": header,
    }

    condition = None
    if isstring(expected[header]):
        checks = [("exact", expected[header])]
    else:
        checks = [(k, v) for (k, v) in sorted(expected[header].items())]

    if isinstance(response, SkippedTest):
        ret["actual"] = exception_actual(response)
        ret["ok"] = True
        ret["skipped"] = True
    elif isinstance(response, Exception):
        ret["actual"] = exception_actual(response)
        ret["ok"] = False
    else:
        try:
            ret["actual"] = response.headers[header]

            ret["ok"] = True
            for (condition, value) in checks:
                if condition == "exact":
                    ret["ok"] = ret["ok"] and response.headers[header] == value
                else:
                    compiled_test = re.compile(value)
                    ret["ok"] = ret["ok"] and bool(
                        compiled_test.search(response.headers[header]))
        except KeyError:
            ret["actual"] = None
            ret["ok"] = False

    return ret


def validate_body(response, expected):
    tests = list(expected.keys())
    if len(tests) != 1:
        print("ERROR: malformed body test")
        sys.exit(1)

    if tests[0] != "regex":
        print(
            "ERROR: currently only regex tests supported on the response body")
        sys.exit(1)

    ret = {
        "expected": expected["regex"],
        "type": "body",
        "skipped": False,
        "variable": "regex",
    }

    if isinstance(response, SkippedTest):
        ret["actual"] = exception_actual(response)
        ret["ok"] = True
        ret["skipped"] = True
    elif isinstance(response, Exception):
        ret["actual"] = exception_actual(response)
        ret["ok"] = False
    else:
        # FIXME: use correct encoding maybe
        actual = response.content.decode("utf-8")
        ret["actual"] = actual[0:40]
        compiled_test = re.compile(expected["regex"], flags=re.MULTILINE | re.DOTALL)
        ret["ok"] = bool(compiled_test.search(actual))

    return ret


def validate_json(response, key, expected):
    ret = {
        "expected": expected,
        "type": "json",
        "skipped": False,
        "variable": key,
    }

    if isinstance(response, SkippedTest):
        ret["actual"] = exception_actual(response)
        ret["ok"] = True
        ret["skipped"] = True
        return ret

    try:
        # FIXME: this reparses json for each check, shouldn't do that.
        actual = response.content.decode("utf-8")
        actualData = dot_json(actual)
    except Exception as e:
        response = e

    if isinstance(response, Exception):
        ret["actual"] = exception_actual(response)
        ret["ok"] = False
    else:
        if key in actualData:
            ret["actual"] = actualData[key]
            ret["ok"] = actualData[key] == expected
        else:
            ret["actual"] = "[no value]"
            ret["ok"] = False

    return ret


def validate_response(test):
    checks = []

    response = test['response']
    if not isinstance(response, Exception):
        # FIXME: the various validate_* functions should be updated to verify using the
        # response object directly.
        serialized = serialize_response(response)
    else:
        serialized = response

    for check, expected in sorted(test["expected"].items()):
        if check == 'exception':
            checks.append(validate_exception(response, expected))
        elif check == "status":
            checks.append(validate_status(serialized, expected))
        elif check == "headers":
            expected = as_iterable(expected)
            for header_test in expected:
                checks.append(validate_header(response, header_test))
        elif check == "body":
            expected = as_iterable(expected)
            for body_test in expected:
                checks.append(validate_body(response, body_test))
        elif check == "json":
            for (key, value) in sorted(expected.items()):
                checks.append(validate_json(response, key, value))
        else:
            print("WARNING: unknown check \"%s\"" % (check))

    test["checks"] = checks


def process_exception(type, value, traceback):
    return value


def run_tests(data, options, user_agent):
    session = requests.Session()
    session.headers.update({'User-Agent': user_agent})

    for test in data["tests"]:
        if "baseUrl" in options:
            test["request"]["baseUrl"] = options["baseUrl"]

        try:
            response = run_test(data["metadata"], test["request"], session)
            test["response"] = response
        except:
            test["response"] = process_exception(*sys.exc_info())

        test["resultDate"] = datetime.datetime.utcnow().isoformat()

        validate_response(test)

    return data


def save(output_root, filename, data):
    failed = 0
    ok = 0
    checks = 0

    data_str = json.dumps(
        data,
        cls=WaldTestEncoder,
        sort_keys=True,
        indent=4,
        separators=(',', ': '))
    with codecs.open(filename, mode="wb", encoding="UTF-8") as f:
        f.write(data_str + "\n")

    for t in data["tests"]:
        for check in t["checks"]:
            checks += 1
            if "ok" in check and check["ok"]:
                ok += 1
            else:
                failed += 1

    print("Saved %s (%d tests, %d failed, %d ok)" %
          (os.path.relpath(filename, os.path.dirname(output_root)), checks,
           failed, ok))


def run_test_file(output_root,
                  test_filename,
                  options={},
                  user_agent='wald:test'):
    output_filename = re.sub('\.test\.json$', '.results.json', test_filename)

    f = codecs.open(test_filename, encoding="UTF-8")
    body = f.read()
    f.close()

    output = run_tests(
        json.loads(body), options=options, user_agent=user_agent)

    save(output_root, output_filename, output)

    return os.path.realpath(output_filename)


if __name__ == "__main__":
    tests = []
    if len(sys.argv) > 1:
        tests = list(map(os.path.realpath, sys.argv[1:]))

    for test_filename in tests:
        if not os.path.isfile(test_filename):
            pass

        if not test_filename.endswith('.test.json'):
            pass

        run_test_file(os.path.realpath("."), test_filename)
