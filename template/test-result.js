/*
 * This file is part of wald:test, A framework for Literate API testing
 * Copyright 2017  Kuno Woudt <kuno@frob.nl>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of copyleft-next 0.3.1.  See copyleft-next-0.3.1.txt
 */

'use strict';

const e = React.createElement;

const entityMap = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#x27;',
    '/': '&#x2F;',
    '`': '&#x60;',
    '=': '&#x3D;',
};

function escapeHtml(string) {
    return String(string).replace(/[&<>"'`=\/]/g, s => entityMap[s]);
}

function statusBadge(ok, skipped, extraClass) {
    if (!ok) {
        return e(
            'span',
            { className: extraClass + ' label label-danger' },
            'FAILED'
        );
    }

    if (skipped) {
        return e(
            'span',
            { className: extraClass + ' label label-warning' },
            'SKIPPED'
        );
    } else {
        return e(
            'span',
            { className: extraClass + ' label label-success' },
            'OK'
        );
    }
}

function lineDrawingFix() {
    // Allow setting line-height: 1 on <pre class="language-php-parent"> in the CSS file.
    const elements = document.getElementsByTagName('code');
    Array.prototype.map.call(elements, (el, idx) => {
        el.classList.forEach(token => {
            if (/^language-/.test(token)) {
                // copy any language tag (e.g. language-php) to the parent (as language-php-parent)
                el.parentNode.classList.add(token + '-parent');
            }
        });
    });
}

function renderHeaders(headers) {
    const output = [];

    for (let key in headers) {
        if (headers.hasOwnProperty(key)) {
            output.push(key + ': ' + headers[key]);
        }
    }

    if (output.length) {
        return output.join('\n') + '\n';
    } else {
        return '';
    }
}

function renderResponse(data) {
    return [
        data.status,
        '\n',
        renderHeaders(data.headers),
        '\n',
        escapeHtml(data.body).replace(/\n$/, ''),
        '\n',
    ].join('');
}

function TestIdentifier(props) {
    const attributes = {
        className: 'btn btn-default test-identifier tab-bar-item',
        disabled: 'disabled',
    };

    // let's make this a button to, so it is easier to style consistently with the tab buttons.
    return e('button', attributes, props.id);
}

function TabButton(props) {
    const attributes = {
        className: 'btn btn-default',
        'data-idx': props.idx,
        onClick: props.onClick,
    };

    if (props.selected) {
        attributes.className += ' active';
    }

    return e('button', attributes, props.title);
}

class Tabs extends React.Component {
    constructor(props) {
        super(props);

        this.handleClick = this.handleClick.bind(this);
        this.state = {};
        this.tabs = ['result', 'expected', 'actual'];
    }

    handleClick(event) {
        event.preventDefault();

        const selected = parseInt(event.target.dataset.idx, 10);
        this.props.handleTabSelect(selected);
    }

    _renderButton(idx) {
        return e(TabButton, {
            onClick: this.handleClick,
            selected: this.props.selected === idx,
            idx: idx,
            title: this.tabs[idx],
        });
    }

    render() {
        if (this.props.expected) {
            return e(
                'div',
                { className: 'btn-group test-result-tabs', role: 'group' },
                e(TestIdentifier, { id: this.props.id }),
                this._renderButton(0),
                this._renderButton(1),
                this._renderButton(2)
            );
        } else {
            return e(
                'div',
                { className: 'test-result-tabs' },
                e(TestIdentifier, { id: this.props.id })
            );
        }
    }
}

class Check extends React.Component {
    render() {
        const r = this.props.result;

        return e(
            'tr',
            { className: r.ok ? 'check-success' : 'check-failed' },
            e('td', { className: 'check-type' }, r.type),
            e('td', { className: 'check-variable' }, r.variable),
            e('td', { className: 'check-ok' }, statusBadge(r.ok, r.skipped)),
            e('td', { className: 'check-expected' }, r.expected),
            e('td', { className: 'check-actual' }, r.actual)
        );
    }
}

class Checks extends React.Component {
    render() {
        const checks = this.props.results.checks;

        return e(
            'table',
            { className: 'check-result-table' },
            e(
                'thead',
                null,
                e(
                    'tr',
                    null,
                    e('th', { className: 'check-type' }, 'check type'),
                    e('th', { className: 'check-variable' }, 'variable'),
                    e('th', { className: 'check-ok' }, 'ok'),
                    e('th', { className: 'check-expected' }, 'expected'),
                    e('th', { className: 'check-actual' }, 'actual')
                )
            ),
            e(
                'tbody',
                null,
                checks.map((c, idx) => {
                    const props = { key: 'check-' + idx, result: c };
                    return e(Check, props);
                })
            )
        );
    }
}

class TocItem extends React.Component {
    render() {
        return e(
            'a',
            { className: this.props.className, href: this.props.href },
            this.props.title,
            e('br'),
            e(
                'span',
                { className: 'text-muted' },
                ' \u2570\u2500 ' + this.props.subtitle
            ),
            this.props.badge
        );
    }
}

class TableOfContents extends React.Component {
    render() {
        const items = this.props.testResults.tests.map(val => {
            const ok = val.checks
                .map(c => c.ok)
                .reduce((memo, val) => memo && val, true);

            const skipped = val.checks
                .map(c => c.skipped)
                .reduce((memo, val) => memo || val, false);

            const props = {
                className: 'list-group-item',
                href: '#' + val.id,
                key: val.id,
                title: val.name,
                subtitle: val.id,
                badge: statusBadge(ok, skipped, 'pull-right'),
            };

            return e(TocItem, props);
        });

        return e('div', { className: 'list-group' }, items);
    }
}

class ProjectIndex extends React.Component {
    render() {
        const items = this.props.index.map(val => {
            // so let's cheat a bit and set skipped false if ok is false
            const skipped = val.ok ? val.skipped : false;

            const props = {
                badge: statusBadge(val.ok, skipped, 'pull-right'),
                className: 'list-group-item',
                href: this.props.rootPath + '/' + val.filename,
                key: val.filename,
                subtitle: val.filename,
                title: val.title,
            };

            // For the project index, failed should trump skipped
            return e(TocItem, props);
        });

        return e('div', { className: 'list-group' }, items);
    }
}

class ResultBox extends React.Component {
    constructor(props) {
        super(props);

        this.handleTabSelect = this.handleTabSelect.bind(this);

        this.state = { selected: 0 };
    }

    handleTabSelect(idx) {
        this.setState({ selected: idx });
    }

    renderTestDescription() {
        const codeBlock = e('div', {
            className: 'code-test-description',
            dangerouslySetInnerHTML: {
                __html: this.props.html,
            },
            ref: el => (this._codeTestDescription = el),
        });

        return e('div', null, e(Tabs, this.props), codeBlock);
    }

    renderResultBox() {
        const data = this.props.testsById[this.props.id];

        const results = e(
            'div',
            {
                className: 'test-results',
                style: {
                    display: this.state.selected === 0 ? 'block' : 'none',
                },
            },
            e('div', { className: 'well' }, e(Checks, { results: data }))
        );

        const expected = e('div', {
            className: 'code-expected',
            dangerouslySetInnerHTML: {
                __html: this.props.html,
            },
            ref: el => (this._codeExpected = el),
            style: { display: this.state.selected === 1 ? 'block' : 'none' },
        });

        // FIXME: truncate the full body here, and add an option to view the raw response in a new
        // tab.  This means writing the raw response to a separate file as raw or basic formatted .html.
        const actual = e(
            'pre',
            {
                className: 'code-actual',
                ref: el => (this._codeActual = el),
                style: {
                    display: this.state.selected === 2 ? 'block' : 'none',
                },
            },
            e('code', {
                className: 'language-http',
                dangerouslySetInnerHTML: {
                    __html: renderResponse(data.response),
                },
            })
        );

        const tabProps = Object.assign(
            {
                handleTabSelect: this.handleTabSelect,
                selected: this.state.selected,
            },
            this.props
        );

        return e('div', null, e(Tabs, tabProps), results, expected, actual);
    }

    render() {
        if (this.props.expected) {
            return this.renderResultBox();
        } else {
            return this.renderTestDescription();
        }
    }
}

class Refresh extends React.Component {
    constructor(props) {
        super(props);

        this.state = { count: 0, metadata: {} };

        if (window._refresh_interval) {
            clearInterval(window._refresh_interval);
        }

        window._refresh_interval = setInterval(() => {
            if (this.state.count > 7) {
                this.liveReload();
                this.setState({ count: 0 });
            } else {
                this.setState({ count: this.state.count + 1 });
            }
        }, 1000);
    }

    liveReload() {
        // check if index.json changed (if so, reload the entire page)

        // cache bust the index.json fetch, because we don't know how the webserver is configured
        // which is serving this file.
        const url = this.props.indexPath + '?t=' + new Date().getTime();

        fetch(url).then(
            response => {
                if (response.ok) {
                    response.json().then(data => {
                        if (data.metadata.run !== this.props.metadata.run) {
                            location.reload(true);
                        }
                    });
                } else {
                    console.log(
                        'ERROR: unable to load index from',
                        this.props.indexPath,
                        response
                    );
                }
            },
            err => {
                console.log(
                    'ERROR: unable to load index from',
                    this.props.indexPath,
                    err
                );
            }
        );
    }

    render() {
        const items = [];
        for (let i = 0; i < 8; i++) {
            const filled = this.state.count > i ? '' : ' filled';

            items.push(
                e('div', {
                    className: 'refresh-tick' + filled,
                    key: 'key' + i,
                })
            );
        }

        return e(
            'ul',
            { className: 'refresh-wrapper list-group' },
            e('li', { className: 'refresh-list list-group-item' }, items)
        );
    }
}

function testResults() {
    // load/parse test results
    const testResults = JSON.parse(
        document.getElementById('test-results').textContent
    );
    const testsById = {};
    testResults.tests.map(val => (testsById[val.id] = val));

    // set line-height: 1 on the <pre> tag surrounding <code class="language-box">, so we can use
    // markdown ```box ... ``` syntax to mark a code snippet for box drawing using unicode line
    // drawing characters.
    lineDrawingFix();

    // table of contents
    ReactDOM.render(
        e(TableOfContents, { testResults: testResults }),
        document.getElementById('wald-test-toc')
    );

    // result boxes
    const elements = document.querySelectorAll('*[data-wald-test]');
    const rendering = Array.prototype.map.call(elements, (codeBlock, idx) => {
        const attributes = {
            expected: codeBlock.dataset.expected === 'true',
            html: codeBlock.innerHTML,
            id: codeBlock.id.replace(/-expected$/, ''),
            testsById: testsById,
        };

        return new Promise((resolve, reject) => {
            ReactDOM.render(e(ResultBox, attributes), codeBlock, resolve);
        });
    });

    // syntax highlight all the <code class="language-foo"> blocks
    Promise.all(rendering).then(() => {
        // FIXME: probably should do this inside the react component, but because
        // we switch between tabs by using display: block vs display: none, the
        // DOM for each tab is never rerendered.  So, for now this is safe.
        hljs.initHighlighting();
    });
}

function index() {
    const rootPath = document.getElementById('test-results').dataset.root;
    const indexPath = rootPath + '/index.json';

    fetch(indexPath).then(
        response => {
            if (response.ok) {
                response.json().then(data => {
                    // project index
                    ReactDOM.render(
                        e(ProjectIndex, {
                            index: data.index,
                            rootPath: rootPath,
                        }),
                        document.getElementById('wald-test-index')
                    );

                    ReactDOM.render(
                        e(Refresh, {
                            indexPath: indexPath,
                            metadata: data.metadata,
                        }),
                        document.getElementById('wald-test-refresh')
                    );
                });
            } else {
                console.log('ERROR: unable to load index from', indexPath);
            }
        },
        err => {
            console.log('ERROR: unable to load index from', indexPath);
        }
    );
}

function main() {
    testResults();
    index();
}

main();
