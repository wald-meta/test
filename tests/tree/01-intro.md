
# Introduction

wald:test allows you to have a tree structure for your tests.  For example, this folder
has the following slightly arbitrary tree to demonstrate this feature:

```box

tree
 │
 ├── 01-intro.md
 ├── 02-ssl
 │    ├── 01-certificates
 │    │   ├── expired.md
 │    │   └── good.md
 │    └── 02-hsts.md
 ├── 03-http-methods
 └── 04-http-headers
```

Some of the tests above contain intentional failing tests, to demonstrate what the
typical output may look like during development, with most tests passing but one or
two failing.



