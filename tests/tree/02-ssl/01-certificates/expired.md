
# Expired Host

We can use badssl to verify that the test runner correctly rejects hosts with
bad certificates.

```yaml
baseUrl: https://expired.badssl.com/
idPrefix: ssl-expired-
```

Expected exception.

```yaml
GET: /
```

```yaml
exception: SSLError
```
