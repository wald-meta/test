
# Elliptic Curve Cryptology

We can use badssl to verify that the test runner correctly accepts hosts using
Elliptic Curve Cryptology digital certificates.

```yaml
baseUrl: https://ecc384.badssl.com/
idPrefix: ssl-ecc-
```

Expected 200 OK

```yaml
GET: /
```

```yaml
status: 200
```

