
# HSTS

We can use badssl to verify that the test runner correctly remembers HSTS, at least
within a session.

```yaml
idPrefix: ssl-hsts-
```

We haven't visited this host yet, so the test runner should return the regular HTTP
response.  Which in this case is an SVG file which indicates that HSTS is not working.

```yaml
baseUrl: http://hsts.badssl.com/
GET: /hsts-test/status.svg
```

```yaml
body:
  - regex: HSTS-is-not-working
```

If we then make a request to the https:// version, which has a
[Strict-Transport-Security](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Strict-Transport-Security)
header set, any subsequent requests for that host should also be sent to https://.

```yaml
baseUrl: https://hsts.badssl.com/
GET: /
```

```yaml
status: 200
headers:
  Strict-Transport-Security:
    regex: max-age
```

Let's try the http:// request again, which should be upgraded by the client to https.
However it seems [python requests](http://docs.python-requests.org/en/master/) doesn't
support HSTS, so this test currently fails.

```yaml
baseUrl: http://hsts.badssl.com/
GET: /hsts-test/status.svg
```

```yaml
body:
  - regex: HSTS-is-working
```

