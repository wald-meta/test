
# HTTP Methods

We can use httpbin to verify that the test runner correctly supports all HTTP methods.

```yaml
baseUrl: https://httpbin.org/
idPrefix: http-methods-
```

#### GET

When requesting /anything, httpbin echoes back what we sent it, including the HTTP method.

```yaml
GET: /anything
skip: true
```

So if we GET /anything, the resulting json body should have method: GET.

We can also try to match the User Agent, however as the version number changes frequently, this
test will fail until some kind of regex support can be implemented for json tests.  (Or the test
file itself is patched every time the version number changes).  So for now this test is marked
as skip: true.


```yaml
status: 200
json:
  method: GET
  headers.User-Agent: wald:test/0.1.0-23 -- https://gitlab.com/wald-meta/test
```

#### HEAD

Let's try HEAD.

```yaml
HEAD: /anything
```

```yaml
status: 200
body:
  regex: ^$
```

#### PATCH

Let's try PATCH.

```yaml
PATCH: /patch
body: This is the patch body.
```

Use JSON for the expected values, so we explictly expect a string for headers.Content-Length.

```json
{
    "status": 200,
    "json": {
        "data": "This is the patch body.",
        "headers.Content-Length": "23"
    }
}
```


