
# HTTP Headers

We can use httpbin to verify that the test runner correctly supports all HTTP methods.

```yaml
baseUrl: https://httpbin.org/
idPrefix: http-headers-
```

When requesting /anything, httpbin echoes back what we sent it, including the HTTP method.


```yaml
GET: /anything
headers:
  Accept: text/turtle;q=0.9,text/html;q=0.7
  X-Wald-Meta: 04-http-headers.md
```

Let's verify that the correct headers were sent.

```yaml
status: 200
json:
  method: GET
  headers.Accept: text/turtle;q=0.9,text/html;q=0.7
  headers.X-Wald-Meta: 04-http-headers.md
```


